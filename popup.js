// --------------------------------------------------------------------------------------------------------------------------
//                                LOGOVÁNÍ VLASTNOSTÍ BAREV + VÝSLEDKŮ SROVNÁVACÍHO TESTU
// --------------------------------------------------------------------------------------------------------------------------

function logColor (c) {

	document.querySelector('.default .hex td:last-child').innerHTML = c.default.hex;
	document.querySelector('.default .rgb td:last-child').innerHTML = c.default.r + ', ' + c.default.g + ', ' + c.default.b;
	document.querySelector('.default .hsl td:last-child').innerHTML = c.default.h + ', '+ c.default.s + ', ' + c.default.l.toFixed(2) + '%';
	document.querySelector('.default .base td:last-child').innerHTML = c.base.hex;
	document.querySelector('.default .lightness td:last-child').innerHTML = c.default.lightness;
	// document.querySelector('.default .contrast td:last-child').innerHTML = c.default.contrast;
	document.querySelector('.default .contrast td:last-child').innerHTML = `${c.default.contrast} <strong>(${Math.round((1 / c.default.contrast)*100)/100}:1)</strong>` ;
	document.querySelector('.default .contrast td:last-child').style.color = `${c.default.contrast < 0.222 ? 'green' : 'red'}` ;
	document.querySelector('.default .pairwith td:last-child').innerHTML = c.default.pairWith;
	document.querySelector('.default .invBase td:last-child').innerHTML = c.default.inverseBase;
	document.querySelector('.default .invContrast td:last-child').innerHTML = c.default.inverseBaseContrast;

	document.querySelector('.lighter .hex td:last-child').innerHTML = c.lighter.hex;
	document.querySelector('.lighter .rgb td:last-child').innerHTML = c.lighter.r + ', ' + c.lighter.g + ', ' + c.lighter.b;
	document.querySelector('.lighter .hsl td:last-child').innerHTML = c.lighter.h + ', '+ c.lighter.s + ', ' + c.lighter.l.toFixed(2) + '%';
	document.querySelector('.lighter .base td:last-child').innerHTML = c.lighter.base;
	document.querySelector('.lighter .lightness td:last-child').innerHTML = c.lighter.lightness;
	// document.querySelector('.lighter .contrast td:last-child').innerHTML = c.lighter.contrast;
	document.querySelector('.lighter .contrast td:last-child').innerHTML = `${c.lighter.contrast} <strong>(${Math.round((1 / c.lighter.contrast)*100)/100}:1)</strong>` ;
	document.querySelector('.lighter .contrast td:last-child').style.color = `${c.lighter.contrast < 0.222 ? 'green' : 'red'}` ;
	document.querySelector('.lighter .pairwith td:last-child').innerHTML = c.lighter.pairWith;
	document.querySelector('.lighter .invBase td:last-child').innerHTML = c.lighter.inverseBase;
	document.querySelector('.lighter .invContrast td:last-child').innerHTML = c.lighter.inverseBaseContrast;

	document.querySelector('.darker .hex td:last-child').innerHTML = c.darker.hex;
	document.querySelector('.darker .rgb td:last-child').innerHTML = c.darker.r + ', ' + c.darker.g + ', ' + c.darker.b;
	document.querySelector('.darker .hsl td:last-child').innerHTML = c.darker.h + ', '+ c.darker.s + ', ' + c.darker.l.toFixed(2) + '%';
	document.querySelector('.darker .base td:last-child').innerHTML = c.darker.base;
	document.querySelector('.darker .lightness td:last-child').innerHTML = c.darker.lightness;
	// document.querySelector('.darker .contrast td:last-child').innerHTML = c.darker.contrast;
	document.querySelector('.darker .contrast td:last-child').innerHTML = `${c.darker.contrast} <strong>(${Math.round((1 / c.darker.contrast)*100)/100}:1)</strong>` ;
	document.querySelector('.darker .contrast td:last-child').style.color = `${c.darker.contrast < 0.222 ? 'green' : 'red'}` ;
	document.querySelector('.darker .pairwith td:last-child').innerHTML = c.darker.pairWith;
	document.querySelector('.darker .invBase td:last-child').innerHTML = c.darker.inverseBase;
	document.querySelector('.darker .invContrast td:last-child').innerHTML = c.darker.inverseBaseContrast;

}

const showTestResults = function(results) {

	let baseSuccessPercentage =  Math.round(results.base.right / (results.base.right + results.base.wrong) * 100);
	let lightnessSuccessPercentage =  Math.round(results.lightness.right / (results.lightness.right + results.lightness.wrong) * 100);
	let defaultPairSuccessPercentage =  Math.round(results.ac.right / (results.ac.right + results.ac.wrong) * 100);
	let darkPairSuccessPercentage =  Math.round(results.acd.right / (results.acd.right + results.acd.wrong) * 100);
	let lightPairSuccessPercentage =  Math.round(results.acl.right / (results.acl.right + results.acl.wrong) * 100);
	let totalScore = baseSuccessPercentage + lightnessSuccessPercentage + defaultPairSuccessPercentage + darkPairSuccessPercentage + lightPairSuccessPercentage;

	document.querySelector('.results .base td:last-child').innerHTML = `<span style='color: green;'>${results.base.right}</span> / <span style='color: red;'>${results.base.wrong}</span> (<strong>${baseSuccessPercentage}%</strong>)`;
	document.querySelector('.results .lightness td:last-child').innerHTML = `<span style='color: green;'>${results.lightness.right}</span> / <span style='color: red;'>${results.lightness.wrong}</span> (<strong>${lightnessSuccessPercentage}%</strong>)`;
	document.querySelector('.results .default-pair td:last-child').innerHTML = `<span style='color: green;'>${results.ac.right}</span> / <span style='color: red;'>${results.ac.wrong}</span> (<strong>${defaultPairSuccessPercentage}%</strong>)`;
	document.querySelector('.results .dark-pair td:last-child').innerHTML = `<span style='color: green;'>${results.acd.right}</span> / <span style='color: red;'>${results.acd.wrong}</span> (<strong>${darkPairSuccessPercentage}%</strong>)`;
	document.querySelector('.results .light-pair td:last-child').innerHTML = `<span style='color: green;'>${results.acl.right}</span> / <span style='color: red;'>${results.acl.wrong}</span> (<strong>${lightPairSuccessPercentage}%</strong>)`;
	document.querySelector('.results .score-sum td:last-child').innerHTML = `<strong>${totalScore} / 500<br />(${Math.round((totalScore / 500) * 100)}%)</strong>`;
}

// --------------------------------------------------------------------------------------------------------------------------
//                                          AKCE PŘI NAČTENÍ ŠABLONY
// --------------------------------------------------------------------------------------------------------------------------
document.addEventListener("DOMContentLoaded", function() {

	console.log("color picker says henlo");

	// ----------------------------------------------------------------------------------------------------------------------
	// načtení stránky - extension se pokusí najít uložené hodnoty a nastavit dle nich inputy v popupu

	 chrome.storage.sync.get(['wtf-colorpicker-data'], function(data){
	    if(typeof data !== 'undefined'){

			console.log("LOADED", data);

			document.querySelector('#color-picker').value = data["wtf-colorpicker-data"].colorHex;
			document.querySelector('#yiq-threshold').value = data["wtf-colorpicker-data"].yiqThreshold;
			document.querySelector('#wcag-threshold').value = data["wtf-colorpicker-data"].wcagThreshold;
			document.querySelector('#dark-threshold').value = data["wtf-colorpicker-data"].darkThreshold;
			document.querySelector('#light-threshold').value = data["wtf-colorpicker-data"].lightThreshold;

			console.log(data["wtf-colorpicker-data"].decisionType);

			document.querySelectorAll('input[name="decision-type"]').forEach(radio => {
				if(radio.value === data["wtf-colorpicker-data"].decisionType){
					radio.setAttribute("checked", true);
				}
			})
		}
	 });

	// --------------------------------------------------------------------------------------------------------------------
	// funkce WORK se volá při jakékoliv změně parametrů v popupu

	const work = function(e){

			// uložení hodnot ze všech inputů do proměnných

			let selectedColorHEX =  document.querySelector('#color-picker').value;
			let decisionTypeValue = document.querySelector('input[name="decision-type"]:checked').value;
			let yiqThresholdValue = parseInt(document.querySelector('#yiq-threshold').value, 10);
			let wcagThresholdValue = parseFloat(document.querySelector('#wcag-threshold').value);
			let darkThresholdValue = parseInt(document.querySelector('#dark-threshold').value, 10);
			let lightThresholdValue = parseInt(document.querySelector('#light-threshold').value, 10);

			// výpis hodnot do local storage

			console.log("PICKED COLOR: " + selectedColorHEX);
			console.log("DECISION TYPE: " + decisionTypeValue);
			console.log("YIQ THRESHOLD: " + yiqThresholdValue)
			console.log("WCAG THRESHOLD: " + wcagThresholdValue)
			console.log("DARK THRESHOLD: " + darkThresholdValue)
			console.log("LIGHT THRESHOLD: " + lightThresholdValue)

			// syncnutí hodnot do local storage

	        chrome.storage.sync.set(
		        {
			        'wtf-colorpicker-data': {
				        colorHex: selectedColorHEX,
				        decisionType: decisionTypeValue,
				        yiqThreshold: yiqThresholdValue,
				        wcagThreshold: wcagThresholdValue,
				        darkThreshold: darkThresholdValue,
				        lightThreshold: lightThresholdValue
			        }
		        }
	        );

			// odeslání message pro content script s předáním všech potřebných dat

	        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
	            chrome.tabs.sendMessage(tabs[0].id, {
	                type:"applyColor",
	                color: selectedColorHEX,
			        decisionType: decisionTypeValue,
			        yiqThreshold: yiqThresholdValue,
			        wcagThreshold: wcagThresholdValue,
			        darkThreshold: darkThresholdValue,
			        lightThreshold: lightThresholdValue
	            }, function(response) {
					// callback sloužící nám pro logování vlastností barvy a test results
	                logColor(response.colorData);
					showTestResults(response.testData);

					console.log(response);
	            })
	        })
	    };

	// -----------------------------------------------------------------------------------------------------------------------------
	//  nabindování funkce WORK na jakoukoliv změnu v inputech popup okna

	["#color-picker", "#yiq-threshold", "#wcag-threshold", "#dark-threshold", "#light-threshold"].forEach((selector) => {
	    document.querySelector(selector).addEventListener("input", work)
	});

    document.querySelectorAll("input[name='decision-type']").forEach(radio => {
		radio.addEventListener("change", work);
    })
});
