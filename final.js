// --------------------------- KONVERZNI FUNKCE ----------------------------------------------------------------------

/**
 * Funkce hexToRGB(...)
 * Převádí hexadecimální zápis barvy do RGB formátu
 * Reference: https://css-tricks.com/converting-color-spaces-in-javascript/#aa-hex-to-rgb
 *
 * @param c {string} string obsahující hexadecimální kód barvy včetně # symbolu
 * @return {object} vrací objekt jehož vlastnosti jsou R, G a B složky dané barvy
 */
function hexToRGB(c) {

    // vytvoření návratového objektu a naplnění defaultními hodnotami
    let color = {
        r: 0,
        g: 0,
        b: 0
    };

    // konverze jednotlivých složek z HEX na RGB
    // funkce umí zpracovat plný zápix (#RRGGBB) i zkrácený zápis (#RGB)
    if (c.length === 4) {
        color.r = parseInt(`0x${c[1]}${c[1]}`, 16);
        color.g = parseInt(`0x${c[2]}${c[2]}`, 16);
        color.b = parseInt(`0x${c[3]}${c[3]}`, 16);
    } else if (c.length === 7) {
        color.r = parseInt(`0x${c[1]}${c[2]}`, 16);
        color.g = parseInt(`0x${c[3]}${c[4]}`, 16);
        color.b = parseInt(`0x${c[5]}${c[6]}`, 16);
    }

    return color;
}

/**
 * Funkce hexToHSL(...)
 * Převádí hexadecimální zápis barvy do HSL formátu
 * Reference: https://css-tricks.com/converting-color-spaces-in-javascript/#aa-hex-to-hsl
 *
 * @param c {string} string obsahující hexadecimální kód barvy včetně # symbolu
 * @return {object} vrací objekt jehož vlastnosti jsou H, S a L složky HSL formátu
 */
function hexToHSL(c) {

    // vytvoření návratového objektu a naplnění defaultními hodnotami
    let color = {
        h: 0,
        s: 0,
        l: 0
    };

    // nejdříve je potřeba převést barvu na RGB model, abychom mohli provádět další výpočty
    let rgbColor = hexToRGB(c);

    // převedeme hodnoty RGB kanálů do rozmezí 0-1
    let r = rgbColor.r / 255;
    let g = rgbColor.g / 255;
    let b = rgbColor.b / 255;

    // ze zjištěných hodnot se zjistí největší a nejnižší hodnota a rozdíl mezi nimi
    let cMin = Math.min(r, g, b);
    let cMax = Math.max(r, g, b);
    let delta = cMax - cMin;

    // definice pomocných proměnných pro výpočty H, S, L
    let h,
        s,
        l;

    // výpočet HUE složky
    if (delta === 0) {
        // mezi RGB složkami není žádný rozdíl
        h = 0;
    } else if (cMax === r) {
        // červená je na maximu
        h = ((g - b) / delta) % 6;
    } else if (cMax === g) {
        // zelená je na maximu
        h = (b - r) / delta + 2;
    } else {
        // modrá ne ja maximu
        h = (r - g) / delta + 4;
    }

    // převod na hodnotu ve stupních vynásobením šedesáti
    h = Math.round(h * 60);

    // pokud je hue negativní, převede se do pozitivních hodnot posunem o 360 stupňů
    if (h < 0) {
        h += 360;
    }

    // výpočet lightness jako polovičního podílu sumy minima a maxima
    l = (cMax + cMin) / 2;

    // výpočet saturation za pomocí delty (rozdílu min/max) a výše vypočítané hodnoty lightness
    // pro delta 0 je vždy výsledek 0, jinak je roven "1 mínus absolutní hodnota dvojnásobku lightness sníženého o 1"
    s = delta === 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));

    // zbývá pouze už jen lightness i saturation vynásobit 100 abychom dostali hodnoty v procentech a zaokrouhlit na desetiny
    s = +(s * 100).toFixed(1);
    l = +(l * 100).toFixed(1);

    // naplnění návratového objektu výsledky a return
    color.h = h;
    color.s = s;
    color.l = l;

    return color;
}

/**
 * Funkce HSLToRGB(...)
 * Převádí barvu z formátu HSL do formátu RGB
 * Reference: https://css-tricks.com/converting-color-spaces-in-javascript/#aa-hsl-to-rgb
 *
 * @param h {number} hue složka HSL formátu
 * @param s {number} saturation složka HSL formátu
 * @param l {number} lightness složka HSL formátu
 * @return {object} vrací objekt jehož vlastnosti jsou R, G a B složky RGB formátu
 */
function HSLToRGB(h, s, l) {

    // vytvoření návratového objektu a naplnění defaultními hodnotami
    let color = {
        r: 0,
        g: 0,
        b: 0
    };

    // saturation a lightness vydělíme 100 abychom je z "procentuálního formátu" dostali do rozmezí 0-1
    s /= 100;
    l /= 100;

    let c = (1 - Math.abs(2 * l - 1)) * s,
        x = c * (1 - Math.abs((h / 60) % 2 - 1)),
        m = l - c / 2,
        r = 0,
        g = 0,
        b = 0;

    if (0 <= h && h < 60) {
        r = c;
        g = x;
        b = 0;
    } else if (60 <= h && h < 120) {
        r = x;
        g = c;
        b = 0;
    } else if (120 <= h && h < 180) {
        r = 0;
        g = c;
        b = x;
    } else if (180 <= h && h < 240) {
        r = 0;
        g = x;
        b = c;
    } else if (240 <= h && h < 300) {
        r = x;
        g = 0;
        b = c;
    } else if (300 <= h && h < 360) {
        r = c;
        g = 0;
        b = x;
    }
    color.r = Math.round((r + m) * 255);
    color.g = Math.round((g + m) * 255);
    color.b = Math.round((b + m) * 255);

    return color;
}

// --------------------------- VYPOCTY VLASTNOSTI ----------------------------------------------------------------------

/**
 * Funkce getLuminance(...)
 * Vypočítá ze zadané RGB barvy její jas
 * Jas v additivních barvách určuje koeficienty pro ac-acl a ac-acd
 * Reference: https://stackoverflow.com/questions/596216/formula-to-determine-perceived-brightness-of-rgb-color
 *
 * @param r {number} červená složka RGB zápisu
 * @param g {number} zelená složka RGB zápisu
 * @param b {number} modrá složka RGB zápisu
 * @return {object} vrací objekt jehož vlastnosti jsou R, G a B složky RGB formátu
 */
function getLuminance(r, g, b) {
    var a = [r, g, b].map(function (v) {
        v /= 255;
        return v <= 0.03928
            ? v / 12.92
            : Math.pow((v + 0.055) / 1.055, 2.4);
    });
    return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;
}

/**
 * Funkce getContrast(...)
 * Vypočítá kontrast (dle WCAG specifikace) mezi vybranou doplňkovou barvou a předanou base color
 * Momentálně má pro nás spíš informační hodnotu a nebude asi potřeba mít jej ve finální verzi
 * Reference: https://dev.to/alvaromontoro/building-your-own-color-contrast-checker-4j7o
 *
 * @param baseRGB {object} objekt base color v RGB formátu
 * @param bgRGB {object} objekt vybrané doplňkové barvy v RGB formátu
 * @return {string} vrací číselnou hodnotu kontrastu mezi danými barvami
 */
function getContrast(baseRGB, bgRGB) {
    // nejdříve se spočítají hodnoty jasu pro obě porovnávané barvy
    let baseLuminance = getLuminance(baseRGB.r, baseRGB.g, baseRGB.b);
    let backgroundLuminance = getLuminance(bgRGB.r, bgRGB.g, bgRGB.b);

    // následně se pomocí zjištěných hodnot jasu vypočítá hodnota kontrastu
    let contrastRatio = baseLuminance > backgroundLuminance
        ? ((backgroundLuminance + 0.05) / (baseLuminance + 0.05))
        : ((baseLuminance + 0.05) / (backgroundLuminance + 0.05));

    return contrastRatio.toFixed(2);
}

/**
 * Funkce getBaseColorYIQ(...)
 * Na základě předané barvy získá kontrastní base color s pomocí 'YIQ' výpočtu
 * Reference: https://24ways.org/2010/calculating-color-contrast/
 *
 * @param color {object} objekt barvy vybrané v color pickeru - v RGB formátu
 * @param threshold {number} číselná hranice rozhodující o finální barvě
 * @return {string} vrátí zkrácený šestnáctkový zápis výsledné base color (#XXX)
 */
function getBaseColorYIQ(color, threshold) { // original threshold is 128
    let yiq = ((color.r * 299) + (color.g * 587) + (color.b * 114)) / 1000;
    return yiq >= threshold ? '#000' : '#fff';
}

/**
 * Funkce getBaseColor(...)
 * Vypočítá nejlepší kontrastní barvu (černá / bílá) oproti vybrané additivní barvě
 * Rozhoduje se dle hodnoty vypočítané dle WCAG specifikace, pokud mají barvy stejný kontrast, rozhodne pomocí YIQ funkce
 * Reference: https://medium.muz.li/the-science-of-color-contrast-an-expert-designers-guide-33e84c41d156
 *
 * @param color {object} objekt barvy vybrané v color pickeru - v RGB formátu
 * @return {object} vrátí objekt se všemi vypočítanými vlastnostmi pro additivku, světlejší odstín, tmavší odstín a base color
 */
function getBaseColor(color) {

    // spočítají se kontrasty pro obě možnosti (bílá base color a černá base color)
    let whiteBase = '#fff';
    let blackBase = '#000';
    let whiteBaseContrast = getContrast({r: 255, g: 255, b: 255}, color);
    let blackBaseContrast = getContrast({r: 0, g: 0, b: 0}, color);
    let finalBaseColor;

    // následně porovnáme kontrasty a vybereme nejlepší možnost
    if (blackBaseContrast < whiteBaseContrast) {
        finalBaseColor = blackBase;
    } else if (whiteBaseContrast < blackBaseContrast) {
        finalBaseColor = whiteBase;
    } else { // equal
        finalBaseColor = getBaseColorYIQ(color, 128);
    }

    return finalBaseColor;
}

/**
 * Funkce getLightness(...)
 * Zjistí světlost dané barvy
 * V kontextu barev v šabloně má lightness jeden jediný účel - pomocí předdefinované (či algoritmicky zjištěné) hodnoty lightness se poté nastaví
 * koeficienty pro zesvětlení a ztmavení LIGHT a DARK odstínu additivní barvy (tyto odstíny se aplikují jako barva pozadí sekce)
 * - pokud je barva vyhodnocená jako DARK, light varianta je o 20% světlejší a dark varianta o 40% tmavší
 * - pokud je barva vyhodnocená jako MEDIUM, light varianta je o 25% světlejší a dark varianta o 20% tmavší
 * - pokud je barva vyhodnocená jako LIGHT, light varianta je o 35% světlejší a dark varianta o 15% tmavší
 *
 * @param color {object} objekt barvy vybrané v color pickeru - v RGB formátu
 * @return {number} vrátí číselnou hodnotu světlosti dané barvy
 */
function getLightness(color) {

	// nejprve výpočet hodnoty jasu pro doplňkovou barvu
	let luminance = getLuminance(color.r, color.g, color.b);
	let lightness;

	// výpočet světlosti pomocí "oficiálního" matematického vzorce
	if (luminance <= (216 / 24389)) {       // The CIE standard states 0.008856 but 216/24389 is the intent for 0.008856451679036
		lightness = luminance * (24389 / 27);  // The CIE standard states 903.3, but 24389/27 is the intent, making 903.296296296296296
	} else {
		lightness = Math.pow(luminance, (1 / 3)) * 116 - 16;
	}
	return lightness;
}

/**
 * Funkce getShades(...)
 * Na základě předané barvy a hodnot thresholdů k barvě vypočítá stupeň její světlosti (DARK / MEDIUM / LIGHT)
 * DLe nich poté nastaví předdefinované koeficienty ztmavení a zesvětlení pro tmavší a světlejší odstín doplňkové barvy
 *
 * @param color {object} objekt barvy vybrané v color pickeru - v RGB formátu
 * @return {object} vrátí objekt s koeficienty pro ztmavení a zesvětlení vybrané doplňkové barvy
 */
function getShades(color) {

	// dark a light threshold definujeme jako konstanty - hodnoty vzesly z testovani FET/DES
	const darkThreshold = 46;
	const lightThreshold = 76;

    // zjisti se hodnota lightness s pomoci getLightness
	let lightness = getLightness(color, darkThreshold, lightThreshold)

    // vytvoření prázdného return objektu pro uložení informací o světlosti a koeficientech ztmavení / zesvětlení
    let colorData = {
        lighter: '',
        darker: ''
    };

    // zde už se jen pomocí zjištěné úrovně lightness doplní do proměnných příslušné koeficienty ztmavení / zesvětlení
    if (lightness < darkThreshold) {
        colorData.lighter = '20';
        colorData.darker = '-40';

    } else if (lightness >= darkThreshold && lightness <= lightThreshold) {
        colorData.lighter = '25';
        colorData.darker = '-20';

    } else if (lightness > lightThreshold) {
        colorData.lighter = '35';
        colorData.darker = '-15';
    }

    return colorData;
}

// --------------------------- HLAVNI FUNKCE -------------------------------------------------------------------------------------------------------------------

/**
 * Funkce createPaletteColor(...)
 * Hlavní funkce, která vezme HEX zápis barvy zvolené v color pickeru a za pomocí výše definovaných pomocných funkcí vypočítá
 * všechny vlastnosti dané barvy.
 *
 * @param c {string} šestnáctkový zápis vybrané barvy
 * @return {object} vrací objekt obsahující všechna potřebná data, rozdělená do "pod-objektů"
 */
function createPaletteColor(c) {

    // definice color objectu, ktery bude obsahovat vsechny potrebne vlastnosti
    let color = {
        ac: {               // data pro vlastní doplňkovou barvu
            hex: c,         // --ac-color
            r: 0,           // z R, G, a B se následně poskládá proměnná --ac-color-rgb (formát "r, g, b")
            g: 0,
            b: 0,
            h: 0,           // --ac-color-h
            s: 0,           // --ac-color-s
            l: 0,           // --ac-color-l
        },
        base: {             // data pro base color
            hex: '',        // --ac-basic
            h: 0,           // --ac-basic-h
            s: 0,           // --ac-basic-s
            l: 0,           // --ac-basic-l
        },
        shades: {           // koeficienty pro ztmavení a zesvětlení doplňkové barvy
            lighter: '',    // --ac-acl
            darker: '',     // --ac-acd
        },
        pairs: {            // párové barvy pro additivní barvu a její světlejší (acl) a tmavší (acd) verzi
            ac: '',         // nepředávají se v podobě CSS proměnných
            acl: '',        // jedná se o hodnoty které využívá přímo CMS při rozhodování jakou párovou barvu použít
            acd: ''
        },
    };

    // zjistime RGB a HSL z obdrzeneho HEX kodu, ze zjistenych hodnot pak spocitame i koeficienty pro svetlejsi a tmavsi odstin pozadi sekce
    color.ac = {...color.ac, ...hexToRGB(c)};
    color.ac = {...color.ac, ...hexToHSL(c)};

	// výpočet potřebných věcí pro base color
	color.base.hex = getBaseColor(color.ac);
	color.base = {...color.base, ...hexToHSL(color.base.hex)};

    // výpočet koeficientů pro zesvětlení a ztmavení
    color.shades = {...color.shades, ...getShades(color.ac)};

    // výpočet párových barev pro chytré sekční additivní barvy
    color.pairs.ac = color.base.hex === '#000' ? 'sc-al' : 'sc-a';
    color.pairs.acl = getBaseColor(HSLToRGB(color.ac.h, color.ac.s, color.ac.l + ((100 - color.ac.l) / 100 * color.shades.lighter))) === '#000' ? 'sc-al' : 'sc-a';
    color.pairs.acd = getBaseColor(HSLToRGB(color.ac.h, color.ac.s,color.ac.l + (color.ac.l / 100 * color.shades.darker))) === '#000' ? 'sc-al' : 'sc-a';

    // ------------ RETURN -------------------------------------

    return color;
}

// https://playcode.io/1249781
console.log(createPaletteColor('#B4D455'));
