
// -------------------------------------------------------------------------------------------------------------------------------------------------------------
//                                                          FUNKCE PRO KONVERZI FORMÁTŮ BAREV
// -------------------------------------------------------------------------------------------------------------------------------------------------------------
//
//  Následující funkce slouží pouze k převodům barevných zápisů.
//
// -------------------------------------------------------------------------------------------------------------------------------------------------------------

/**
 * Funkce RGBToHex(...)
 * Převádí RGB zápis barvy do hexadecimálního formátu
 * Reference: https://css-tricks.com/converting-color-spaces-in-javascript/#aa-rgb-to-hex
 *
 * @param r {number} červená složka RGB zápisu
 * @param g {number} zelená složka RGB zápisu
 * @param b {number} modrá složka RGB zápisu
 * @return {string} vrací string s hexadecimálním kódem barvy (#XXXXXX), vždy v šesticiferném formátu
 */
function RGBToHex(r,g,b) {

	let convertChannel = (c) => {
		let _c = c.toString(16);
		return _c.length === 1 ? `0${_c}` : _c;
	};

	return `#${convertChannel(r)}${convertChannel(g)}${convertChannel(b)}`;

	// let _r = r.toString(16);
	// let _g = g.toString(16);
	// let _b = b.toString(16);
	//
	// _r = _r.length === 1 ? `0${_r}` : _r;
	// _g = _g.length === 1 ? `0${_g}` : _g;
	// _b = _b.length === 1 ? `0${_b}` : _b;

	// return "#" + r + g + b;
}

/**
 * Funkce hexToRGB(...)
 * Převádí hexadecimální zápis barvy do RGB formátu
 * Reference: https://css-tricks.com/converting-color-spaces-in-javascript/#aa-hex-to-rgb
 *
 * @param c {string} string obsahující hexadecimální kód barvy včetně # symbolu
 * @return {object} vrací objekt jehož vlastnosti jsou R, G a B složky dané barvy
 */
function hexToRGB (c) {

	// vytvoření návratového objektu a naplnění defaultními hodnotami
	let color = {
		r: 0,
		g: 0,
		b: 0,
	}

	// konverze jednotlivých složek z HEX na RGB
	// funkce umí zpracovat plný zápix (#RRGGBB) i zkrácený zápis (#RGB)
	if (c.length === 4) {
		color.r = parseInt(`0x${c[1]}${c[1]}`, 16);
		color.g = parseInt(`0x${c[2]}${c[2]}`, 16);
		color.b = parseInt(`0x${c[3]}${c[3]}`, 16);
	} else if (c.length === 7) {
		color.r = parseInt(`0x${c[1]}${c[2]}`, 16);
		color.g = parseInt(`0x${c[3]}${c[4]}`, 16);
		color.b = parseInt(`0x${c[5]}${c[6]}`, 16);
	}

	// console.log(`HEX TO RGB: ${c} > `, color);

	return color;
}

/**
 * Funkce hexToHSL(...)
 * Převádí hexadecimální zápis barvy do HSL formátu
 * Reference: https://css-tricks.com/converting-color-spaces-in-javascript/#aa-hex-to-hsl
 *
 * @param c {string} string obsahující hexadecimální kód barvy včetně # symbolu
 * @return {object} vrací objekt jehož vlastnosti jsou H, S a L složky HSL formátu
 */
function hexToHSL (c) {

	// vytvoření návratového objektu a naplnění defaultními hodnotami
	let color = {
		h: 0,
		s: 0,
		l: 0,
	}

	// nejdříve je potřeba převést barvu na RGB model, abychom mohli provádět další výpočty
	let rgbColor = hexToRGB(c);

	// převedeme hodnoty RGB kanálů do rozmezí 0-1
	let r = rgbColor.r / 255;
	let g = rgbColor.g / 255;
	let b = rgbColor.b / 255;

	// ze zjištěných hodnot se zjistí největší a nejnižší hodnota a rozdíl mezi nimi
	let cMin = Math.min(r,g,b);
	let	cMax = Math.max(r,g,b);
	let	delta = cMax - cMin;

	// definice pomocných proměnných pro výpočty H, S, L
	let	h, s, l;

	// výpočet HUE složky
	if (delta === 0) {
		// mezi RGB složkami není žádný rozdíl
		h = 0;
	} else if (cMax === r) {
		// červená je na maximu
		h = ((g - b) / delta) % 6;
	} else if (cMax === g) {
		// zelená je na maximu
		h = (b - r) / delta + 2;
	} else {
		// modrá ne ja maximu
		h = (r - g) / delta + 4;
	}

	// převod na hodnotu ve stupních vynásobením šedesáti
	h = Math.round(h * 60);

	// pokud je hue negativní, převede se do pozitivních hodnot posunem o 360 stupňů
	if (h < 0) { h += 360; }

	// výpočet lightness jako polovičního podílu sumy minima a maxima
	l = (cMax + cMin) / 2;

	// výpočet saturation za pomocí delty (rozdílu min/max) a výše vypočítané hodnoty lightness
	// pro delta 0 je vždy výsledek 0, jinak je roven "1 mínus absolutní hodnota dvojnásobku lightness sníženého o 1"
	s = delta === 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));

	// zbývá pouze už jen lightness i saturation vynásobit 100 abychom dostali hodnoty v procentech a zaokrouhlit na desetiny
	s = +(s * 100).toFixed(1);
	l = +(l * 100).toFixed(1);

	// naplnění návratového objektu výsledky a return
	color.h = h;
	color.s = s;
	color.l = l;

	return color;
}

/**
 * Funkce HSLToRGB(...)
 * Převádí barvu z formátu HSL do formátu RGB
 * Reference: https://css-tricks.com/converting-color-spaces-in-javascript/#aa-hsl-to-rgb
 *
 * @param h {number} hue složka HSL formátu
 * @param s {number} saturation složka HSL formátu
 * @param l {number} lightness složka HSL formátu
 * @return {object} vrací objekt jehož vlastnosti jsou R, G a B složky RGB formátu
 */
function HSLToRGB(h,s,l) {

	// vytvoření návratového objektu a naplnění defaultními hodnotami
	let color = {
		r: 0,
		g: 0,
		b: 0,
	}

	// saturation a lightness vydělíme 100 abychom je z "procentuálního formátu" dostali do rozmezí 0-1
	s /= 100;
	l /= 100;

	let c = (1 - Math.abs(2 * l - 1)) * s,
		x = c * (1 - Math.abs((h / 60) % 2 - 1)),
		m = l - c/2,
		r = 0,
		g = 0,
		b = 0;

	if (0 <= h && h < 60) {
		r = c;
		g = x;
		b = 0;
	} else if (60 <= h && h < 120) {
		r = x;
		g = c;
		b = 0;
	} else if (120 <= h && h < 180) {
		r = 0;
		g = c;
		b = x;
	} else if (180 <= h && h < 240) {
		r = 0;
		g = x;
		b = c;
	} else if (240 <= h && h < 300) {
		r = x;
		g = 0;
		b = c;
	} else if (300 <= h && h < 360) {
		r = c;
		g = 0;
		b = x;
	}
	color.r = Math.round((r + m) * 255);
	color.g = Math.round((g + m) * 255);
	color.b = Math.round((b + m) * 255);

	return color;
}

// --------------------------- VYPOCTY VLASTNOSTI --------------------------------------------------------------------------------------------------------------

/**
 * Funkce getLuminance(...)
 * Vypočítá ze zadané RGB barvy její jas
 * Jas v additivních barvách určuje koeficienty pro ac-acl a ac-acd
 * Reference: https://stackoverflow.com/questions/596216/formula-to-determine-perceived-brightness-of-rgb-color
 *
 * @param r {number} červená složka RGB zápisu
 * @param g {number} zelená složka RGB zápisu
 * @param b {number} modrá složka RGB zápisu
 * @return {object} vrací objekt jehož vlastnosti jsou R, G a B složky RGB formátu
 */
function getLuminance(r, g, b) {
	var a = [r, g, b].map(function (v) {
		v /= 255;
		return v <= 0.03928
			? v / 12.92
			: Math.pow( (v + 0.055) / 1.055, 2.4 );
	});
	return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;
}

/**
 * Funkce getContrast(...)
 * Vypočítá kontrast mezi vybranou doplňkovou barvou a předanou base color
 * Momentálně má pro nás spíš informační hodnotu a nebude asi potřeba mít jej ve finální verzi
 * Reference: https://dev.to/alvaromontoro/building-your-own-color-contrast-checker-4j7o
 *
 * @param baseRGB {object} objekt base color v RGB formátu
 * @param bgRGB {object} objekt vybrané doplňkové barvy v RGB formátu
 * @return {string} vrací číselnou hodnotu kontrastu mezi danými barvami
 */
function getContrast (baseRGB, bgRGB) {
	// nejdříve se spočítají hodnoty jasu pro obě porovnávané barvy
	let baseLuminance = getLuminance(baseRGB.r, baseRGB.g, baseRGB.b);
	let backgroundLuminance = getLuminance(bgRGB.r, bgRGB.g, bgRGB.b);

	// následně se pomocí zjištěných hodnot jasu vypočítá hodnota kontrastu
	let contrastRatio = baseLuminance > backgroundLuminance
		? ((backgroundLuminance + 0.05) / (baseLuminance + 0.05))
		: ((baseLuminance + 0.05) / (backgroundLuminance + 0.05));

	return contrastRatio.toFixed(2);
}


// function getBaseColor (color, threshold) { // original threshold is 128
// 	let yiq = ((color.r*299)+(color.g*587)+(color.b*114))/1000;
// 	return (yiq >= threshold) ? '#000' : '#fff';
// }

/**
 * Funkce getBaseColorYIQ(...)
 * Na základě předané barvy získá kontrastní base color s pomocí 'YIQ' výpočtu
 * Reference: https://24ways.org/2010/calculating-color-contrast/
 *
 * @param color {object} objekt barvy vybrané v color pickeru - v RGB formátu
 * @param threshold {number} číselná hranice rozhodující o finální barvě
 * @return {string} vrátí zkrácený šestnáctkový zápis výsledné base color (#XXX)
 */
function getBaseColorYIQ (color, threshold) { // original threshold is 128
	let yiq = ((color.r * 299) + (color.g * 587) + (color.b * 114)) / 1000;
	return yiq >= threshold ? '#000' : '#fff';
}

/**
 * Funkce getBaseColorWCAG(...)
 * Vypočítá kontrast mezi vybranou doplňkovou barvou a předanou base color s pomocí WCAG metriky
 * Může v sobě kombinovat i YIQ metodu z předchozí funkce
 * Tuto funkci ve výsledném řešení nejspíše nevyužíjeme, YIQ metoda má výsledky blíže našim současným barvám
 * Reference: https://medium.muz.li/the-science-of-color-contrast-an-expert-designers-guide-33e84c41d156
 *
 * @param color {object} objekt barvy vybrané v color pickeru - v RGB formátu
 * @param decisionType {string} flag rozhodující o použitých metodách výpočtů
 * @param wcagColorThreshold {number}  číselná hranice rozhodující o finální barvě při použití WCAG metriky
 * @param yiqColorThreshold {number}  číselná hranice rozhodující o finální barvě při dodatečném použití YIQ metody
 * @return {string} vrátí zkrácený šestnáctkový zápis výsledné base color (#XXX)
 */
function getBaseColorWCAG (color, decisionType, wcagColorThreshold, yiqColorThreshold) {

	if(decisionType === "wcag") {
		// spočítají se kontrasty pro obě možnosti (bílá base color a černá base color)
		let whiteBaseColorContrast = getContrast({ r: 255, g: 255, b: 255 }, color);
		let blackBaseColorContrast = getContrast({ r: 0, g: 0, b: 0 }, color);

		// vrátí se možnost s vyšším kontrastem vůči pozadí
		// nebylo zde mysleno na případ kdy jsou kontrasty stejné - v takovém případě se použije černá
		return whiteBaseColorContrast < blackBaseColorContrast ? "#fff" : "#000"

	}else if (decisionType === "both") {
		let computedBaseColor = getBaseColorYIQ(color, yiqColorThreshold);
		let inverseBaseColor = computedBaseColor === '#000' ? '#fff' : '#000';
		let computedContrast = getContrast(hexToRGB(computedBaseColor), color);
		let computedInverseContrast = getContrast(hexToRGB(inverseBaseColor), color);
		let chosenBaseColor;

		if(computedContrast <= wcagColorThreshold) {
			chosenBaseColor = computedBaseColor;
		}else if(computedInverseContrast <= wcagColorThreshold) {
			chosenBaseColor = inverseBaseColor;
		}else{
			// ani jedna barva nevyhovuje zadane podmince
			if(computedContrast === computedInverseContrast) {
				chosenBaseColor = computedBaseColor;
			}else{
				chosenBaseColor = computedContrast < computedInverseContrast ? computedBaseColor : inverseBaseColor;
			}
		}

		return chosenBaseColor;

		// return computedContrast <= wcagColorThreshold ? (computedBaseColor === '#000' ? '#000' : '#fff') : (computedBaseColor === '#000' ? '#fff' : '#000');
	}
}

function getBaseColor (color) {


		// spočítají se kontrasty pro obě možnosti (bílá base color a černá base color)

		let whiteBase = '#fff';
		let blackBase = '#000';
		let whiteBaseContrast = getContrast({ r: 255, g: 255, b: 255 }, color);
		let blackBaseContrast = getContrast({ r: 0, g: 0, b: 0 }, color);
		let finalBaseColor;

		if (blackBaseContrast < whiteBaseContrast) {
			finalBaseColor = blackBase;
		} else if (whiteBaseContrast < blackBaseContrast) {
			finalBaseColor = whiteBase;
		} else { // equal
			finalBaseColor = getBaseColorYIQ(color, 128);
		}

		return finalBaseColor;
}

// -------------------------------------------------------------------------------------------------------------------------------------------------------------
//	                                                          LIGHTNESS (SVĚTLOST)
// -------------------------------------------------------------------------------------------------------------------------------------------------------------
//
//	 V kontextu barev v šabloně má lightness jeden jediný účel - pomocí předdefinované (či algoritmicky zjištěné) hodnoty lightness se poté nastaví
//	 koeficienty pro zesvětlení a ztmavení LIGHT a DARK odstínu additivní barvy (tyto odstíny se aplikují jako barva pozadí sekce)
//
//	- pokud je barva vyhodnocená jako DARK, light varianta je o 20% světlejší a dark varianta o 40% tmavší
//	- pokud je barva vyhodnocená jako MEDIUM, light varianta je o 25% světlejší a dark varianta o 20% tmavší
//	- pokud je barva vyhodnocená jako LIGHT, light varianta je o 35% světlejší a dark varianta o 15% tmavší
//
// -------------------------------------------------------------------------------------------------------------------------------------------------------------

/**
 * Funkce getLightness(...)
 * Na základě předané barvy a hodnot thresholdů k barvě vypočítá stupeň její světlosti (DARK / MEDIUM / LIGHT)
 * DLe nich poté nastaví předdefinované koeficienty ztmavení a zesvětlení pro tmavší a světlejší odstín doplňkové barvy
 * Reference:
 *
 * @param color {object} objekt barvy vybrané v color pickeru - v RGB formátu
 * @param darkThreshold {number} threshold rozlišující MEDIUM od DARK
 * @param lightThreshold {number} threshold rozlišující LIGHT od MEDIUM
 * @return {string} vrátí zkrácený šestnáctkový zápis výsledné base color (#XXX)
 */
function getLightness (color, darkThreshold, lightThreshold) {

	// nejprve výpočet hodnoty jasu pro doplňkovou barvu
	let luminance = getLuminance(color.r, color.g, color.b);
	let lightness;

	// výpočet světlosti pomocí "oficiálního" matematického vzorce
    if (luminance <= (216/24389)) {       // The CIE standard states 0.008856 but 216/24389 is the intent for 0.008856451679036
        lightness = luminance * (24389/27);  // The CIE standard states 903.3, but 24389/27 is the intent, making 903.296296296296296
    } else {
        lightness = Math.pow(luminance,(1/3)) * 116 - 16;
    }

	// vytvoření prázdného return objektu pro uložení informací o světlosti a koeficientech ztmavení / zesvětlení
	let colorData = {
		lightness: '',
		acl: '',
		acd: '',
	}

	// zde už se jen pomocí zjištěné úrovně lightness doplní do proměnných příslušné koeficienty ztmavení / zesvětlení
	if (lightness < darkThreshold) {
		colorData.lightness = 'dark';
		colorData.acl = '20';
		colorData.acd = '-40';
	} else if (lightness >= darkThreshold && lightness <= lightThreshold) {
		colorData.lightness = 'medium';
		colorData.acl = '25';
		colorData.acd = '-20';
	} else if (lightness > lightThreshold) {
		colorData.lightness = 'light';
		colorData.acl = '35';
		colorData.acd = '-15';
	}

	return colorData;
}

// // pozustatek z predchozi iterace, nejspise uz nebude potreba
// function getPairColor (color, baseColorThreshold) {
// 	let pairColor;
// 	let computedBaseColor = getBaseColor(color, baseColorThreshold); //  === '#000' ? 'sc-al' : 'sc-a';
// 	let computedContrast = getContrast(computedBaseColor, color);
//
// 	if(computedContrast < 0.22222){
// 		pairColor = computedBaseColor === '#000' ? 'sc-al' : 'sc-a';
// 	}else{
// 		pairColor = computedBaseColor === '#000' ? 'sc-a' : 'sc-al';
// 	}
//
// 	return pairColor;
// }

// --------------------------- HLAVNI FUNKCE -------------------------------------------------------------------------------------------------------------------

/**
 * Funkce createColorFromHex(...)
 * Hlavní funkce, která vezme HEX zápis barvy zvolené v color pickeru a za pomocí výše definovaných pomocných funkcí vypočítá
 * všechny vlastnosti dané barvy.
 *
 * @param c {string} šestnáctkový zápis vybrané barvy
 * @param decisionType {string} threshold rozlišující MEDIUM od DARK
 * @param yiqThreshold {number} hodnota thresholdu pro YIQ výpočet base color
 * @param wcagThreshold {number} hodnota thresholdu pro WCAG výpočet base color
 * @param darkThreshold {number} threshold pro světlost rozlišující MEDIUM od DARK
 * @param lightThreshold {number} threshold pro světlost rozlišující LIGHT od MEDIUM
 * @return {object} vrací objekt obsahující v sobě objekt s vlastnostmi barvy a také druhý objekt s výsledky testů
 */
function createColorFromHex(c, decisionType, yiqThreshold, wcagThreshold, darkThreshold, lightThreshold) {

	// ----------- DEFINE COLOR OBJECT ------------------------

	// console.log("CREATE COLOR FROM HEX, ", c);

	let color = {
		default: {
			hex: c,
			r: 0,
			g: 0,
			b: 0,
			h: 0,
			s: 0,
			l: 0,
			contrast: 0,
			lightness: '',
			acl: '',
			acd: '',
			pairWith: '',
			inverseBase: '',
			inverseBaseContrast: '',
		},
		lighter: {
			hex: c,
			r: 0,
			g: 0,
			b: 0,
			h: 0,
			s: 0,
			l: 0,
			contrast: 0,
			lightness: '',
			acl: '',
			acd: '',
			pairWith: '',
			base: '',
			inverseBase: '',
			inverseBaseContrast: '',
		},
		darker: {
			hex: c,
			r: 0,
			g: 0,
			b: 0,
			h: 0,
			s: 0,
			l: 0,
			contrast: 0,
			lightness: '',
			acl: '',
			acd: '',
			pairWith: '',
			base: '',
			inverseBase: '',
			inverseBaseContrast: '',
		},
		// base color se vztahuje pouze k defaultni barve
		base: {
			hex: '',
			r: 0,
			g: 0,
			b: 0,
			h: 0,
			s: 0,
			l: 0,
		},
	}

	// ----------- GET RGB VALUES ------------------------------

	let rgb = hexToRGB(c);

	color.default.r = rgb.r;
	color.default.g = rgb.g;
	color.default.b = rgb.b;

	// ----------- GET HSL VALUES ------------------------------

	let hsl = hexToHSL(c);

	color.default.h = hsl.h;
	color.default.s = hsl.s;
	color.default.l = hsl.l;

	// ------------ COMPUTING COLOR-PALETTE.YML PARAMS --------

	const baseColorThreshold = yiqThreshold;
	// const baseColorThreshold = 204; // nase defaultni hodnota
	// const baseColorThreshold = 128; // defaultni hodnota

	// BASE COLOR

	// if(decisionType === "yiq") {
	// 	// YIQ
	// 	color.base.hex = getBaseColorYIQ(color.default, baseColorThreshold);
	// }else {
	// 	// WCAG / WCAG + YIQ
	// 	color.base.hex = getBaseColorWCAG(color.default, decisionType, wcagThreshold, baseColorThreshold);
	// }
	color.base.hex = getBaseColor(color.default);

	// ADDITIVE COLOR LIGHTNESS

	// let darkThreshold = 40;
	// let lightThreshold = 64;

	let lightnessData = getLightness(color.default, darkThreshold, lightThreshold);
	color.default.lightness = lightnessData.lightness;
	color.default.acl = lightnessData.acl;
	color.default.acd = lightnessData.acd;

	// ADDITIVE COLOR VS BASE COLOR CONTRAST

	let baseRGB = hexToRGB(color.base.hex);
	let baseHSL = hexToHSL(color.base.hex);

	color.base.h = baseHSL.h;
	color.base.s = baseHSL.s;
	color.base.l = baseHSL.l;

	color.default.contrast = getContrast(baseRGB, color.default);

	// sc-a jsou svetle elementy, sc-al jsou tmave elementy
	color.default.pairWith = color.base.hex === '#000' ? 'sc-al' : 'sc-a';
	// color.default.pairWith = getPairColor(color.default, baseColorThreshold);

	color.default.inverseBase = color.base.hex === '#000' ? '#fff' : '#000';
	color.default.inverseBaseContrast = getContrast(hexToRGB(color.default.inverseBase), color.default);

	// ---- LIGHTER SHADE BG ----------------------------------------------

	color.lighter.h = color.default.h;
	color.lighter.s = color.default.s;
	color.lighter.l = color.default.l + ((100 - color.default.l) / 100 * color.default.acl);

	let lighterRGB = HSLToRGB(color.lighter.h, color.lighter.s, color.lighter.l);

	color.lighter.r = lighterRGB.r;
	color.lighter.g = lighterRGB.g;
	color.lighter.b = lighterRGB.b;

	color.lighter.hex = RGBToHex(lighterRGB.r, lighterRGB.g, lighterRGB.b);

	let lighterLightnessData = getLightness(color.lighter, darkThreshold, lightThreshold);
	color.lighter.lightness = lighterLightnessData.lightness;
	color.lighter.acl = lighterLightnessData.acl;
	color.lighter.acd = lighterLightnessData.acd;

	// if(decisionType === "yiq") {
	// 	// YIQ
	// 	color.lighter.base = getBaseColorYIQ(color.lighter, baseColorThreshold);
	// }else {
	// 	// WCAG / WCAG + YIQ
	// 	color.lighter.base = getBaseColorWCAG(color.lighter, decisionType, wcagThreshold, baseColorThreshold);
	// }
	color.lighter.base = getBaseColor(color.lighter);

	color.lighter.contrast = getContrast(hexToRGB(color.lighter.base), color.lighter);

	color.lighter.inverseBase = color.lighter.base === '#000' ? '#fff' : '#000';
	color.lighter.inverseBaseContrast = getContrast(hexToRGB(color.lighter.inverseBase), color.lighter);

	// sc-a jsou svetle elementy, sc-al jsou tmave elementy
	color.lighter.pairWith = color.lighter.base === '#000' ? 'sc-al' : 'sc-a';
	// color.lighter.pairWith = getBaseColor(color.lighter, baseColorThreshold) === '#000' ? 'sc-al' : 'sc-a';
	// color.lighter.pairWith = getPairColor(color.lighter, baseColorThreshold);

	// ---- DARKER SHADE BG ----------------------------------------------

	color.darker.h = color.default.h;
	color.darker.s = color.default.s;
	color.darker.l = color.default.l + (color.default.l / 100 * color.default.acd);

	let darkerRGB = HSLToRGB(color.darker.h, color.darker.s, color.darker.l);

	color.darker.r = darkerRGB.r;
	color.darker.g = darkerRGB.g;
	color.darker.b = darkerRGB.b;

	color.darker.hex = RGBToHex(darkerRGB.r, darkerRGB.g, darkerRGB.b);

	let darkerLightnessData = getLightness(color.darker, darkThreshold, lightThreshold);
	color.darker.lightness = darkerLightnessData.lightness;
	color.darker.acl = darkerLightnessData.acl;
	color.darker.acd = darkerLightnessData.acd;

	// if(decisionType === "yiq") {
	// 	// YIQ
	// 	color.darker.base = getBaseColorYIQ(color.darker, baseColorThreshold);
	// }else {
	// 	// WCAG / WCAG + YIQ
	// 	color.darker.base = getBaseColorWCAG(color.darker, decisionType, wcagThreshold, baseColorThreshold);
	// }
	color.darker.base = getBaseColor(color.darker);

	color.darker.contrast = getContrast(hexToRGB(color.darker.base), color.darker);

	color.darker.inverseBase = color.darker.base === '#000' ? '#fff' : '#000';
	color.darker.inverseBaseContrast = getContrast(hexToRGB(color.darker.inverseBase), color.darker);

	// sc-a jsou svetle elementy, sc-al jsou tmave elementy
	color.darker.pairWith = color.darker.base === '#000' ? 'sc-al' : 'sc-a';
	// color.darker.pairWith = getBaseColor(color.darker, baseColorThreshold) === '#000' ? 'sc-al' : 'sc-a';
	// color.darker.pairWith = getPairColor(color.darker, baseColorThreshold);

	// ------------ RETURN -------------------------------------

	return color;
}

// --------------------------- MANIPULACE S DOMEM
// ---------------------------------------------------------------------------------------------------------------

var applyColor = function(color, decisionType, yiqThreshold, wcagThreshold, darkThreshold, lightThreshold){

	// console.log("APPLYING COLOR: " + color);
	// console.log("DECISION TYPE: " + decisionType);
	// console.log("YIQ THRESHOLD: " + yiqThreshold);
	// console.log("WCAG THRESHOLD: " + wcagThreshold);
	// console.log("DARK THRESHOLD: " + darkThreshold);
	// console.log("LIGHT THRESHOLD: " + lightThreshold);

	const c = createColorFromHex(color, decisionType, yiqThreshold, wcagThreshold, darkThreshold, lightThreshold);
	let root = document.documentElement;

	root.style.setProperty('--ac-color', c.default.hex);
	root.style.setProperty('--ac-color-rgb', `${c.default.r}, ${c.default.g}, ${c.default.b}`);
	root.style.setProperty('--ac-color-h', `${c.default.h}`);
	root.style.setProperty('--ac-color-s', `${c.default.s}%`);
	root.style.setProperty('--ac-color-l', `${c.default.l}%`);
	root.style.setProperty('--ac-acl', c.default.acl);
	root.style.setProperty('--ac-acd', c.default.acd);
	root.style.setProperty('--ac-basic', c.base.hex);
	root.style.setProperty('--ac-basic-h', `${c.base.h}`);
	root.style.setProperty('--ac-basic-s', `${c.base.s}%`);
	root.style.setProperty('--ac-basic-l', `${c.base.l}%`);

	// vymena parovych trid

	document.querySelectorAll('.sc-ac')?.forEach((section) => {
		section.classList.remove('sc-ml', 'sc-m', 'sc-a', 'sc-al');
		section.classList.add(c.default.pairWith);
	})
	document.querySelectorAll('.sc-acl')?.forEach((section) => {
		section.classList.remove('sc-ml', 'sc-m', 'sc-a', 'sc-al');
		section.classList.add(c.lighter.pairWith);
	})
	document.querySelectorAll('.sc-acd')?.forEach((section) => {
		section.classList.remove('sc-ml', 'sc-m', 'sc-a', 'sc-al');
		section.classList.add(c.darker.pairWith);
	})

	let testResults = {
		lightness: {
			right: 0,
			wrong: 0,
		},
		base: {
			right: 0,
			wrong: 0,
		},
		ac: {
			right: 0,
			wrong: 0,
		},
		acl: {
			right: 0,
			wrong: 0,
		},
		acd: {
			right: 0,
			wrong: 0,
		}
	}

	testArray.forEach((testedColor) => {

		// console.log("TEST TEST TEST");
		// console.log(testedColor);



		const tColor = createColorFromHex(testedColor.color, decisionType, yiqThreshold, wcagThreshold, darkThreshold, lightThreshold);

		// base color test

		if(tColor.base.hex === testedColor.base){
			testResults.base.right += 1;
		}else {
			testResults.base.wrong += 1;
		}

		// lightness test

		if(tColor.default.lightness === testedColor.lightness){
			testResults.lightness.right += 1;
		}else {
			testResults.lightness.wrong += 1;
		}

		// ac pair color test

		if(tColor.default.pairWith === testedColor.ac){
			testResults.ac.right += 1;
		}else {
			testResults.ac.wrong += 1;
		}

		// acl pair color test

		if(tColor.lighter.pairWith === testedColor.acl){
			testResults.acl.right += 1;
		}else {
			testResults.acl.wrong += 1;
		}

		// acd pair color test

		if(tColor.darker.pairWith === testedColor.acd){
			testResults.acd.right += 1;
		}else {
			testResults.acd.wrong += 1;
		}

	});

	// console.log("COLOR", c);
	// console.log("TESTS", testResults);

	return {
		colorData: c,
		testData: testResults
	};
};

chrome.runtime.onMessage.addListener(
    function(message, sender, sendResponse) {
        switch(message.type) {
            case "applyColor":
                let response = applyColor(
					message.color,
	                message.decisionType,
	                message.yiqThreshold,
	                message.wcagThreshold,
	                message.darkThreshold,
	                message.lightThreshold
                );
				console.log(response);
                sendResponse(response);
                break;
        }
    }
);

$(document).ready(function(){
	 chrome.storage.sync.get(['wtf-colorpicker-data'], function(data){
		 console.log("LOADED CONTENT");
		 if(typeof data["wtf-colorpicker-data"] !== 'undefined'){
	        applyColor(
				data["wtf-colorpicker-data"].colorHex,
		        data["wtf-colorpicker-data"].decisionType,
		        data["wtf-colorpicker-data"].yiqThreshold,
		        data["wtf-colorpicker-data"].wcagThreshold,
		        data["wtf-colorpicker-data"].darkThreshold,
		        data["wtf-colorpicker-data"].lightThreshold
	        );
		 };
	 });
});
